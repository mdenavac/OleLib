// Package.
package com.OleLib;

//Imports.
import java.util.TreeMap;

/*-----------------------------------------------------------------------------
 * 
 *  Class : SfMetaDatas.
 *  
 *  Role :  Static class of information about SF Meta Datas Types.
 *  
 ----------------------------------------------------------------------------*/
public class SfMetaDatas
{
    /**
     * Attributes.
     */
    private static TreeMap<String, SfMetaDataType> metaDatas = null;
    
    /*-------------------------------------------------------------------------
     * PUBLIC METHODS.
     ------------------------------------------------------------------------*/
    /**
     * Return the meta data associated to the
     * file type.
     */
    public static String getSfMeta(final String fileName, Boolean retPkgXml)
    {
        if (fileName == null || fileName.isEmpty())
            return null;
        
        if (metaDatas == null)
            SfMetaDatas.initSfMetaDataTypes();
        
        retPkgXml = (retPkgXml == null) ? false : retPkgXml;
        for (String s : metaDatas.keySet())
        {
            if (fileName.matches(s))
            {
                if (retPkgXml)
                    return metaDatas.get(s).pkgXmlName;
                else
                    return metaDatas.get(s).dirName;
            }
        }
        
        return null;
    }
    
    /*-------------------------------------------------------------------------
     * PRIVATE METHODS.
     ------------------------------------------------------------------------*/
    /**
     * Initialize the SF MetaDatas types.
     */
    private static void initSfMetaDataTypes()
    {
        metaDatas = new TreeMap<String, SfMetaDataType>();
        metaDatas.put("(.*)app$", new SfMetaDataType("applications", "CustomApplication"));
        metaDatas.put("(.*)cls(-meta.xml)?$", new SfMetaDataType("classes", "ApexClass"));
        metaDatas.put("(.*)component(-meta.xml)?$", new SfMetaDataType("components", "ApexComponent"));
        metaDatas.put("(.*)email(-meta.xml)?$", new SfMetaDataType("email", "EmailTemplate"));
        metaDatas.put("(.*)group$", new SfMetaDataType("groups", "Group"));
        metaDatas.put("(.*)homePageComponent$", new SfMetaDataType("homePageComponents", "HomePageComponent"));
        metaDatas.put("(.*)homePageLayout$", new SfMetaDataType("homePageLayouts", "HomePageLayout"));
        metaDatas.put("(.*)labels$", new SfMetaDataType("labels", "CustomLabels"));
        metaDatas.put("(.*)layout$", new SfMetaDataType("layouts", "Layout"));
        metaDatas.put("(.*)letter$", new SfMetaDataType("letterhead", ""));
        metaDatas.put("(.*)object$", new SfMetaDataType("objects", "CustomObject"));
        metaDatas.put("(.*)objectTranslation$", new SfMetaDataType("objectTranslations", "CustomObjectTranslation"));
        metaDatas.put("(.*)page(-meta.xml)?$", new SfMetaDataType("pages", "ApexPage"));
        metaDatas.put("(.*)profile$", new SfMetaDataType("profiles", ""));
        metaDatas.put("(.*)remoteSite$", new SfMetaDataType("remoteSiteSettings", ""));
        metaDatas.put("(.*)reportType$", new SfMetaDataType("reportTypes", ""));
        metaDatas.put("(.*)role$", new SfMetaDataType("roles", ""));
        metaDatas.put("(.*)resource(-meta.xml)?$", new SfMetaDataType("staticresources", "StaticResource"));
        metaDatas.put("(.*)tab", new SfMetaDataType("tabs", "CustomTab"));
        metaDatas.put("(.*)territory$", new SfMetaDataType("territories", ""));
        metaDatas.put("(.*)translation$", new SfMetaDataType("translations", "Translations"));
        metaDatas.put("(.*)trigger(-meta.xml)?$", new SfMetaDataType("triggers", "ApexTrigger"));
        metaDatas.put("(.*)weblink$", new SfMetaDataType("weblinks", ""));
        metaDatas.put("(.*)workflow$", new SfMetaDataType("workflows", "Workflow"));
    }
}
