// Package.
package com.OleLib;

// Imports.
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.File;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.tools.ant.Task;
import org.xml.sax.InputSource;
import org.w3c.dom.NodeList;

/*-----------------------------------------------------------------------------
 * 
 *  Class : RepoExtract.
 *  
 *  Role :  Extract files from a repository.
 *  
 ----------------------------------------------------------------------------*/
public class RepoExtract extends Task
{
    /**
     * Task properties.
     */
    /* diffFilePath */
    private String diffFilePath;
    public void setDiffFilePath(final String value)
    {
        this.diffFilePath = value;
    }
    
    /* extractPath */
    private String extractPath;
    public void setExtractPath(final String value)
    {
        this.extractPath = value;
    }
    
    /* repoCommand */
    private String repoCommand;
    public void setRepoCommand(final String value)
    {
        this.repoCommand = value;
    }
    
    /*-------------------------------------------------------------------------
     * PUBLIC METHODS.
     ------------------------------------------------------------------------*/
    /**
     * execute().
     * Entry point of the Ant task.
     */
    public void execute()
    {
        this.extract();
        this.buildPackageXml();
    }
    
    /*-------------------------------------------------------------------------
     * PRIVATE METHODS.
     ------------------------------------------------------------------------*/
    /**
     * Extract files from a repository according to the diff file.
     */
    private void extract()
    {
        // Open the diff file.
        InputSource diffFile = null;
        try
        {
            diffFile = new InputSource(new FileInputStream(diffFilePath));
        }
        catch (FileNotFoundException fnfe)
        {
            System.out.println("Diff file not found : " + diffFilePath);
            return;
        }
        
        // Parse file using XPath.
        NodeList nodes = null;
        try
        {
            XPathExpression exp = XPathFactory.newInstance().newXPath().compile("/diff/paths/path/text()");
            nodes = (NodeList)exp.evaluate(diffFile, XPathConstants.NODESET);
        }
        catch(XPathExpressionException xpee)
        {
            System.out.println("Error using XPath");
            return;
        }
        
        // Extract files.
        String value = null;
        for (int i = 0; i < nodes.getLength(); i++)
        {
            value = nodes.item(i).getNodeValue();
            
            // Extract the file.
            this.doRepoExtract(value);

            // Extract the meta file.
            if (value.endsWith("cls") || value.endsWith("component") ||
                value.endsWith("email") || value.endsWith("page") ||
                value.endsWith("trigger") || value.endsWith("resource"))
            {
                this.doRepoExtract(value + "-meta.xml");
            }
        }
    }
    
    /**
     * Execute the Repo export of the file.
     * @param filename
     */
    private void doRepoExtract(final String filename)
    {
        // Create the directory structure.
        if (filename.endsWith("src"))
        {
            return;
        }
        String extractPath = this.extractPath + filename.substring(filename.lastIndexOf("src") + 3, filename.lastIndexOf("/") +  1);
        File f = new File(extractPath);
        f.mkdirs();

        // Extract from the repository.
        try
        {
            String cmd = repoCommand.replace("FILETODOWNLOAD", filename);
            cmd = cmd.replace("EXTRACTPATH", extractPath);
            Runtime.getRuntime().exec(cmd).waitFor();
        }
        catch (InterruptedException ie)
        {
            System.out.println("Extract interrupted " + filename);
        }
        catch (Exception e)
        {
            System.out.println("Failed to extract " + filename);
        }
    }
    
    /**
     * Build the package.xml file.
     */
    private void buildPackageXml()
    {
        // Build a map of files per type.
        TreeMap<String, ArrayList<String>> tree = new TreeMap<String, ArrayList<String>>();
        this.parseDirectory(new File(this.extractPath), tree);
        
        // Write the package.xml file.
        File packageFile = new File(this.extractPath + "/package.xml");
        BufferedWriter packageBuf = null;
        try
        {
            packageFile.createNewFile();
            packageBuf = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(packageFile)));
        }
        catch (IOException ioe)
        {
            System.out.println("Unable to create the package.xml file");
            return;
        }
        
        try
        {
            packageBuf.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            packageBuf.newLine();
            packageBuf.write("<package xmlns=\"http://soap.sforce.com/2006/04/metadata\">");
            
            for (String n : tree.keySet())
            {
                // <types>
                packageBuf.newLine();
                packageBuf.write("<types>");
                
                // ..<name></name>
                packageBuf.newLine();
                packageBuf.write("<name>" + n + "</name>");
                
                // ....<members></members>
                for (String m : tree.get(n))
                {
                    packageBuf.newLine();
                    packageBuf.write("<members>" + m + "</members>");
                }
                
                // </types>
                packageBuf.newLine();
                packageBuf.write("</types>");
            }
            
            packageBuf.newLine();
            packageBuf.write("<version>29.0</version>");
            
            packageBuf.newLine();
            packageBuf.write("</package>");
            packageBuf.close();
        }
        catch (IOException ioe)
        {
            System.out.println("Unable to write into package.xml file");
        }
    }
    
    /**
     * Parse a directory and fill the tree Map.
     * @param dir
     * @param tree
     */
    private void parseDirectory(File dir, TreeMap<String, ArrayList<String>> tree)
    {
        String fileName = null;
        String pkgName = null;
        
        // List files by SF type.
        for (File f : dir.listFiles())
        {
            if (f.isDirectory())
            {
                this.parseDirectory(f, tree);
            }
            else
            {
                fileName = f.getName();
                
                // By pass all meta files.
                if (fileName.contains("meta"))
                {
                    continue;
                }
                
                // Get the package name.
                pkgName = SfMetaDatas.getSfMeta(fileName, true);
                if (pkgName == null)
                {
                    System.out.println("Unknow package name for file " + fileName);
                    continue;
                }
                
                // Create an entry for this SF type of component.
                if (!tree.containsKey(pkgName))
                {
                    tree.put(pkgName, new ArrayList<String>());
                }
                
                // Add the file name to the list of this type.
                tree.get(pkgName).add(this.rmExtension(fileName));
            }
        }
    }
    
    /**
     * Return the filename without extension.
     */
    private String rmExtension(final String entry)
    {
        for (int i = entry.length() - 1; i != 0; i--)
        {
            if (entry.charAt(i) == '.')
            {
                return entry.substring(0, entry.length() - (entry.length() - i));
            }
        }
        return null;
    }
}
