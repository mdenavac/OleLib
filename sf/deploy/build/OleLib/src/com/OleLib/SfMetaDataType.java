package com.OleLib;

/*-----------------------------------------------------------------------------
 * 
 *  Class : SfMetaDataType.
 *  
 *  Role :  SF Meta Data Type.
 *  
 ----------------------------------------------------------------------------*/
class SfMetaDataType
{
    /**
     * Attributes.
     */
    public String dirName;
    public String pkgXmlName;
    
    /*-------------------------------------------------------------------------
     * PUBLIC METHODS.
     ------------------------------------------------------------------------*/
    /**
     * Constructor.
     */
    public SfMetaDataType(final String dirName, final String pkgXmlName)
    {
        this.dirName = dirName;
        this.pkgXmlName = pkgXmlName;
    }
}