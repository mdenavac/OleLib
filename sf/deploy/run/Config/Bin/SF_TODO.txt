1) Download Salesforce Ant Task from https://login.salesforce.com.
	-> Setup > Develop > Tools

2) Unzip the file in the current directory.

3) Cut & paste the 'ant-salesforce' file to ant\lib directory.

4) Delete the extract directory.