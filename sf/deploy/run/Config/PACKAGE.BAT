@ECHO OFF

REM
REM Define the environment.
REM
CALL SET_ENV.BAT

REM
REM Prepare the target.
REM
CLS
ECHO +-----------------------------------------------------------------------------+
ECHO ^|                                                                             ^|
ECHO ^|                 PREPARING TARGET                                            ^|
ECHO ^|                                                                             ^|
ECHO +-----------------------------------------------------------------------------+

REM Define variables.
SET VERSION_HOME=%1
SET VERSION_PATH=%VERSION_HOME%\Package_%DATE:~0,2%-%DATE:~3,2%-%DATE:~6,4%_%TIME:~0,2%h%TIME:~3,2%m%TIME:~6,2%s
SET PROPERTIES_FILENAME=%2

REM Copy the version properties file.
DEL .\%PROPERTIES_FILENAME%
XCOPY %VERSION_HOME%\%PROPERTIES_FILENAME% .\ /Y

REM Create the final version path and update
REM the version.properties file.
MKDIR "%VERSION_PATH%"
ECHO version_path=%VERSION_PATH:\=/%/>> .\%PROPERTIES_FILENAME%

REM
REM Start the packaging.
REM
CLS
ECHO +-----------------------------------------------------------------------------+
ECHO ^|                                                                             ^|
ECHO ^|                 PACKAGING FILES                                             ^|
ECHO ^|                                                                             ^|
ECHO +-----------------------------------------------------------------------------+

CALL %ANT_EXEC% Package -buildfile package.xml
DEL %VERSION_HOME%\Package.zip
XCOPY "%VERSION_PATH%\Package.zip" %VERSION_HOME%

REM Debug :
REM PAUSE